import json
import re

import numpy as np
from PyQt5.QtGui import QColor
from qgis.core import QgsSymbol, QgsSvgMarkerSymbolLayer, QgsSettings, QgsMessageLog, Qgis
from PyQt5.QtCore import pyqtSignal, QObject

from .jaro_winkler import jaro_winkler_similarity
from .path_utils import create_existing_icon_path, get_spatial_entities_path, load_entity_embedding_json

DEFAULT_ICON_SIZE = 2

MIN_JARO_WINKLER_SIMILARITY = 0.75


def load_spatial_entities():
    with open(get_spatial_entities_path(), 'r') as file:
        data = json.load(file)
    return data


class Matcher(QObject):
    step_signal = pyqtSignal()

    def __init__(self, api_key: str = None, language: str = 'English'):
        super().__init__()
        self.spatial_entities = load_spatial_entities()
        self.api_key = api_key
        self.language = language
        self.spatial_entity_embeddings = None
        self.has_embeddings = []
        self.has_no_embeddings = []
        self.settings = QgsSettings()
        self.log_enabled = self.settings.value("/plugins/AIAMAS/log_enabled", False, bool)

    def match(self, layers):
        for layer in layers:
            if layer.embedding is not None:
                self.has_embeddings.append(layer)
            else:
                self.has_no_embeddings.append(layer)
            self.step_signal.emit()

        if len(self.has_embeddings) > 0:
            log_messages = self.match_embeddings(self.has_embeddings)
            if self.log_enabled:
                QgsMessageLog.logMessage(
                    "Verbose log of AIAMAS style suggestions using LLM-based semantic similarity.\n" + 
                    "Similarity; Layer name; Entity name; Color suggestions; Icon suggestions" + 
                    "".join(log_messages) + "\n", 
                    "AIAMAS", 
                    Qgis.Info
                )


        elif len(self.has_no_embeddings) > 0:
            log_messages = self.match_by_jarowinkler(self.has_no_embeddings)
            if self.log_enabled:
                QgsMessageLog.logMessage(
                    "Verbose log of AIAMAS style suggestions using Jaro-Winkler similarity.\n" + 
                    "Similarity; Layer name; Entity name; Color suggestions; Icon suggestions" + 
                    "".join(log_messages) + "\n", 
                    "AIAMAS", 
                    Qgis.Info
                )
        else:
            for layer in layers:
                self.step_signal.emit()  

    def match_by_jarowinkler(self, layers):
        log_messages = []
        for index, layer in enumerate(layers):
            layer_name_parts = re.split(r"[\W_]+", layer.normalized_name)
            layer_name_parts.append(layer.normalized_name)
            best_match = 0
            lang_method = None
            colors = None
            icons = None

            if self.language == 'English':
                lang_method = match_en
            if self.language == 'German':
                lang_method = match_de
            if self.language == 'French':
                lang_method = match_fr

            for layer_name_part in layer_name_parts:
                for entity in self.spatial_entities['data']:
                    match = lang_method(entity, layer_name_part)

                    if match > best_match:
                        best_match = match
                        colors = hex_string_to_qcolor(entity['color'])
                        icons = entity['icons']

            set_style(layer, colors, icons, best_match)
            if self.log_enabled:
                log_messages.append(f"\n{round(best_match, 2)}; {layer.name}; {', '.join(entity['name_en'])}; {', '.join(entity['color'])}; {', '.join(entity['icons'])}")
                                
            self.step_signal.emit()
        return log_messages

    def match_embeddings(self, layers):
        log_messages = []
        self.spatial_entity_embeddings = load_entity_embedding_json("https://api.llmhub.infs.ch/embedding", "intfloat/multilingual-e5-large-instruct")

        if self.spatial_entity_embeddings is None or len(self.spatial_entity_embeddings) == 0:
            return

        for layer in layers:
            best_match: str = ""
            best_similarity = 0
            for key, value in self.spatial_entity_embeddings.items():
                similarity = cosine_similarity(layer.embedding, value)
                if similarity > best_similarity:
                    best_similarity = similarity
                    best_match = key
                    if best_similarity >= 1:
                        break

            if best_match is not None:
                style_set = False
                for entity in self.spatial_entities['data']:
                    for lang in ["name_en", "name_de", "name_fr"]:
                        for entity_name in entity[lang]:
                            if entity_name == best_match and style_set == False:
                                set_style(layer, hex_string_to_qcolor(entity['color']), entity['icons'])
                                style_set = True
                                if self.log_enabled:
                                    log_messages.append(f"\n{round(best_similarity, 2)}; {layer.name}; {', '.join(entity['name_en'])}; {', '.join(entity['color'])}; {', '.join(entity['icons'])}")
                                break
                        if style_set:
                            break
            self.step_signal.emit()
        return log_messages


def cosine_similarity(emb1, emb2):
    dot_product = np.dot(emb1, emb2)
    norm_emb1 = np.linalg.norm(emb1)
    norm_emb2 = np.linalg.norm(emb2)
    return dot_product / (norm_emb1 * norm_emb2)


def set_style(layer, colors, icons, best_match=None):
    layer.suggested_colors = colors
    layer.suggested_icons = icons

    layer.suggested_symbol = QgsSymbol.defaultSymbol(layer.qgs_layer.geometryType())
    layer.suggested_colors.insert(0, layer.qgs_layer.renderer().symbol().color())
    if best_match is None or best_match > MIN_JARO_WINKLER_SIMILARITY:
        layer.suggested_symbol.changeSymbolLayer(0, QgsSvgMarkerSymbolLayer(
            str(create_existing_icon_path(layer.suggested_icons[0])), DEFAULT_ICON_SIZE + 5, 0))
        layer.suggested_icon_path = str(layer.suggested_icons[0])
        layer.suggested_symbol.setColor(layer.suggested_colors[1])
    else:
        layer.is_default_symbol = True
        layer.suggested_symbol.setColor(layer.suggested_colors[0])


def match_en(entity, layer_name_part, cnt=0):
    matches = []
    for name in entity['name_en']:
        matches.append(jaro_winkler_similarity(layer_name_part.lower(), name.lower()))

    match = max(matches)
    if match < 0.5 and cnt <= 2:
        match = match_fr(entity, layer_name_part, (cnt + 1))
    return match


def match_de(entity, layer_name_part, cnt=0):
    matches = []
    for name in entity['name_de']:
        matches.append(jaro_winkler_similarity(layer_name_part.lower(), name.lower()))

    match = max(matches)
    if match < 0.5 and cnt <= 2:
        match = match_en(entity, layer_name_part, (cnt + 1))
    return match


def match_fr(entity, layer_name_part, cnt=0):
    matches = []
    for name in entity['name_fr']:
        matches.append(jaro_winkler_similarity(layer_name_part.lower(), name.lower()))

    match = max(matches)
    if match < 0.5 and cnt <= 2:
        match = match_de(entity, layer_name_part, (cnt + 1))
    return match


def hex_string_to_qcolor(hex_string):
    qcolor = []
    for i in range(len(hex_string)):
        string = hex_string[i].lstrip('#')

        # Extracting the RGB values
        r = int(string[0:2], 16)
        g = int(string[2:4], 16)
        b = int(string[4:6], 16)

        # Creating a QColor object
        qcolor.append(QColor(r, g, b))

    return qcolor
