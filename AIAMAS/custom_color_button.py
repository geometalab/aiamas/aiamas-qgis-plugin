from qgis.gui import QgsColorButton


class CustomColorButton(QgsColorButton):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setShowMenu(False)
        self.clicked.disconnect()

    def dragEnterEvent(self, event):
        pass

    def dragLeaveEvent(self, event):
        pass

    def dragMoveEvent(self, event):
        pass

    def dropEvent(self, event):
        pass

    def mouseMoveEvent(self, event):
        pass
