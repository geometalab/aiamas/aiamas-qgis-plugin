from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QToolButton
from qgis.PyQt.QtGui import *
from qgis.gui import QgsSymbolButton


class CustomSymbolButton(QgsSymbolButton):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setPopupMode(QToolButton.DelayedPopup)
        self.setMenu(None)
        self.clicked.disconnect()

    def dragEnterEvent(self, event):
        pass

    def dragLeaveEvent(self, event):
        pass

    def dragMoveEvent(self, event):
        pass

    def dropEvent(self, event):
        pass

    def wheelEvent(self, event):
        pass

    def mouseMoveEvent(self, event):
        pass

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            super().mousePressEvent(event)
        else:
            pass

    def acceptDrops(self):
        pass
