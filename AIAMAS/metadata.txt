# Mandatory items:

[general]
name=AIAMAS - AI-Assisted Map Styler
qgisMinimumVersion=3.16.6
qgisMaximumVersion=3.99
version=0.3.0

author=Diego Fantino, Niklas Vogel, Samuel Meuli, Stefan Keller
email=geometalab@ost.ch

description=Symbolize your vector layers in seconds

about=This plugin allows quick symbolization and automatically suggests fitting colors and symbols for newly loaded vector layers. It does not replace the QGIS Style Manager. 
    Vector layer names are assigned to the most similar entity name (e.g. bank, restaurant) using a list of predefined entities with associated styles. The suggested styles - i.e. colors and symbols - can be applied as is, or customized with a few mouse clicks. 
    To match layer names to entity names a Large Language Model (LLM) is used for semantic similarity. If no LLM service is available, the plugin defaults to algorithmic string similarity methods.
    The plugin uses an open source LLM service hosted by the Institute for Software (IFS) at the Eastern Switzerland University of Applied Sciences (OST). To use this plugin, an API token must be entered in the Options dialog. This token can be generated for free on the IFS LLMHub website https://llmhub.infs.ch/. By using this service, you agree to follow its policy. The plugin primarily handles layer names in English, German and French. Help improve this plugin. Submit an issue or a merge request.

homepage=https://gitlab.com/geometalab/aiamas/aiamas-qgis-plugin/-/blob/main/README.md
tracker=https://gitlab.com/geometalab/aiamas/AIAMAS-QGIS-Plugin/-/issues
repository=https://gitlab.com/geometalab/aiamas/AIAMAS-QGIS-Plugin

# Recommended items:

tags=style, vector, layers, color, icon, machine learning
icon=data/icon.png
category=Vector
hasProcessingProvider=no
experimental=True
server=False
deprecated=False

changelog=
    0.3.0 2024-09-13: All Layers supported, QGIS Styles are considered in AIAMAS
    0.2.0 2024-07-17: Add more spatial entity embeddings, fix dev script argument, refactor dev scripts
    0.1.7 2024-07-12: Fix f-string syntax, correct dates in changelog, slightly change log output
    0.1.6 2024-07-12: Changes in verbose log
    0.1.5 2024-07-12: Fix path issue
    0.1.4 2024-06-20: Update metadata
    0.1.3 2024-05-24: Minimal Changes in User Interface
    0.1.2 2024-05-07: First experimental version.
