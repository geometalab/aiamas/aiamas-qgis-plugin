import json
import os
from pathlib import Path

from qgis.core import QgsApplication

QGIS_SVGPATH, = Path(QgsApplication.applicationFilePath()).parent.parent.glob('**/qgis/svg')
OWN_SVGPATH = Path(QgsApplication.qgisSettingsDirPath()) / "svg"
PATH = Path(os.path.dirname(os.path.abspath(__file__)))

# TODO: Add error handling if path doesn't exist
def create_existing_icon_path(file_name: str):
    clean_qgis_svg_path = QGIS_SVGPATH / file_name
    clean_plugin_svg_path = OWN_SVGPATH / file_name

    if os.path.exists(clean_plugin_svg_path):
        existing_svg_path = clean_plugin_svg_path
    else:
        existing_svg_path = clean_qgis_svg_path

    return existing_svg_path


def get_spatial_entities_path():
    spatial_entities_path = os.path.join(
        os.path.dirname(__file__),
        'data/spatial_entities_colors_icons.json')

    return spatial_entities_path

def load_entity_embedding_json(END_POINT, LLM_MODEL):
    path = PATH / "data/entity_embeddings.json"

    if os.path.exists(path):
        with open(path, 'r') as file:
            data = json.load(file)
            if "_llm_url" in data and "_llm_model" in data and "entity_embeddings" in data:
                if data["_llm_url"] == END_POINT and data["_llm_model"] == LLM_MODEL:
                    data = data["entity_embeddings"]
                else:
                    data = {}
            else:
                data = {}
    else:
        data = {}
    return data
