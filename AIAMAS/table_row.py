from dataclasses import dataclass
from PyQt5.QtGui import QColor
from qgis.PyQt.QtWidgets import QCheckBox, QHBoxLayout, QWidget, QLabel, QTableWidgetItem
from qgis.PyQt.QtCore import Qt
from .vectorlayer import VectorLayer, symbolize

from .custom_color_button import CustomColorButton
from .custom_symbol_button import CustomSymbolButton
from dataclasses import dataclass

from PyQt5.QtGui import QColor
from qgis.PyQt.QtCore import Qt
from qgis.PyQt.QtWidgets import QCheckBox, QHBoxLayout, QWidget, QLabel, QTableWidgetItem

from .custom_color_button import CustomColorButton
from .custom_symbol_button import CustomSymbolButton
from .vectorlayer import VectorLayer, symbolize


@dataclass
class TableRow():
    checkbox: QCheckBox = None
    checkbox_container: QWidget = None
    name_field: QLabel = None
    color_field: CustomColorButton = None
    icon_field: CustomSymbolButton = None
    vectorlayer: VectorLayer = None

    needs_update: bool = False

    @classmethod
    def create_table_rows_after_loading(cls, q_layers):
        layers = VectorLayer.add_layers_after_loading(q_layers)
        return [cls.create_table_row(layer) for layer in layers]

    @classmethod
    def create_table_rows_from_qgis_project(cls):
        layers = VectorLayer.get_all_from_qgs_project()
        return [cls.create_table_row(layer) for layer in layers]

    @classmethod
    def create_table_row(cls, layer: VectorLayer):
        checkbox = QCheckBox()
        checkbox_container = cls.ready_up_checkbox(checkbox)
        name_field = cls.create_name_field(layer)
        color_field = cls.create_color_field(layer)
        icon_field = cls.create_icon_field(layer)

        new_table_row = cls(checkbox=checkbox, checkbox_container=checkbox_container, name_field=name_field,
                            color_field=color_field, icon_field=icon_field, vectorlayer=layer)

        new_table_row.configure_CheckBox()
        layer.qgs_layer.nameChanged.connect(new_table_row.handle_name_change)
        
        # Connect to the styleChanged signal
        layer.qgs_layer.styleChanged.connect(new_table_row.handle_style_change)

        return new_table_row

    @classmethod
    def update_table_rows(cls, table_rows):
        table_rows_to_update = []
        for table_row in table_rows:
            if table_row.needs_update:
                table_rows_to_update.append(table_row)
                table_row.needs_update = False

        symbolize([table_row.vectorlayer for table_row in table_rows_to_update])

        for table_row in table_rows_to_update:
            table_row.update_table_row()

    def handle_style_change(self):
        """Update the color and symbol when the layer's style changes."""
        self.update_color_fild()
        self.update_icon_fild()
        self.update_chooser_widget_colors()

    def update_chooser_widget_colors(self):
        """Update the color options in the ChooserWidget."""
        if hasattr(self, 'chooser_widget') and self.chooser_widget.is_color:
            self.chooser_widget.update_colors(self.vectorlayer.suggested_symbol.color())

    def handle_name_change(self):
        self.needs_update = True

    def update_table_row(self):
        self.update_layer_name()
        self.update_color_fild()
        self.update_icon_fild()

    def update_layer_name(self):
        self.name_field.setText(str(self.vectorlayer.name))

    def update_color_fild(self):
        """Update the color button with the current color from the layer's style."""
        self.color_field.setColor(self.vectorlayer.qgs_layer.renderer().symbol().color())

    def update_icon_fild(self):
        """Update the symbol button with the current symbol from the layer's style."""
        self.icon_field.setLayer(self.vectorlayer.qgs_layer)
        cloned_symbol = self.vectorlayer.qgs_layer.renderer().symbol().clone()
        self.icon_field.setSymbol(cloned_symbol)

    def select_signal_detected(self):
        return self.checkbox.setChecked(True)

    def unselect_signal_detected(self):
        return self.checkbox.setChecked(False)

    def configure_CheckBox(self):
        self.checkbox.setChecked(True)

        self.checkbox.stateChanged.connect(lambda state: self.update_layer_name_color(state == Qt.Checked))
        self.checkbox.stateChanged.connect(lambda state: self.vectorlayer.change_checked(self.checkbox.isChecked()))

    @staticmethod
    def ready_up_checkbox(checkbox: QCheckBox):
        layout = QHBoxLayout()
        layout.addWidget(checkbox)
        layout.setAlignment(checkbox, Qt.AlignCenter)

        cell_widget = QWidget()
        cell_widget.setLayout(layout)

        return cell_widget

    @staticmethod
    def create_name_field(layer: VectorLayer):
        return QTableWidgetItem(str(layer.name))

    @staticmethod
    def create_color_field(layer: VectorLayer):
        color_selector = CustomColorButton()
        color_selector.setColor(layer.suggested_symbol.color())

        return color_selector

    @staticmethod
    def create_icon_field(layer: VectorLayer):
        icon_selector = CustomSymbolButton()
        icon_selector.setLayer(layer.qgs_layer)
        cloned_symbol = layer.suggested_symbol.clone()
        icon_selector.setSymbol(cloned_symbol)

        return icon_selector

    def update_layer_name_color(self, checked: bool):
        if self.name_field:
            color = QColor("black" if checked else "gray")
            self.name_field.setForeground(color)
