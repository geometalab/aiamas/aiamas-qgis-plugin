"""
    from https://github.com/yjg30737/pyqt-svg-button/blob/a8e257b1c7fdba116a507d9220d1e6755d5663d7/pyqt_svg_button/svgButton.py
"""

from PyQt5.QtWidgets import QPushButton, QWidget

from .pyqt_svg_abstractbutton import SvgAbstractButton


class SvgButton(QPushButton, SvgAbstractButton):
    def __init__(self, base_widget: QWidget = None, *args, **kwargs):
        super().__init__(base_widget, *args, **kwargs)
