# AIAMAS - AI-Assisted Map Styler 

## Short description

This plugin allows quick symbolization and automatically suggests fitting colors and symbols for newly loaded vector layers. It does not replace the QGIS Style Manager.<br>
Vector layer names are assigned to the most similar entity name (e.g. bank, restaurant) using a list of predefined entities with associated styles. The suggested styles - i.e. colors and symbols - can be applied as is, or customized with a few mouse clicks.<br>
To match layer names to entity names a Large Language Model (LLM) is used for semantic similarity. If no LLM service is available, the plugin defaults to algorithmic string similarity methods.<br>
The plugin uses an open source LLM service hosted by the Institute for Software (IFS) at the Eastern Switzerland University of Applied Sciences (OST). To use this plugin, an API token must be entered in the Options dialog. This token can be generated for free on the IFS LLMHub website https://llmhub.infs.ch/ . By using this service, you agree to follow its policy. The plugin primarily handles layer names in English, German and French. Help improve this plugin. Submit an issue or a merge request.

Tip: The [QGIS plugin "Adjust Style"](https://plugins.qgis.org/plugins/qgis_adjust_style/) allows you to change the symbology of a map with a few clicks instead of changing each symbol individually.


## Installing the plugin locally 

If you want to install the plugin locally go to the [releases](https://gitlab.com/geometalab/aiamas/aiamas-qgis-plugin/-/releases).
There download the AIAMAS_QGIA_PLUGIN.zip and import it to QGIS.


## Help

There is a [Help Page](https://geometalab.gitlab.io/aiamas/aiamas-qgis-plugin/help/), which is the one displayed when you click on the Help button of the AIAMAS dialog.


## Installing the plugin for development

### Setup

1. Clone the repository to your workspace.
2. Take the AIAMAS folder and make a link to your plugins folder in ```AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins\<foldername>```. You can do this by Opening a terminal in the AIAMAS folder and running the following command:
```mklink /D <path_to_plugins_folder> <full path to aiamas folder>```
3. Open QGIS and enable the AIAMAS plugin.

The script folder contains scripts that are used by the developers. It has its own readme.md

### Development

For development, you will have to open the two folders "aiamas" and "scripts" in different projects because the aiamas folder uses the QGIS Python environment and the scripts folder uses the Poetry environment.

If you want a more detailed explanation of the AIAMAS plugin and how it works, you can find it in the [Documentation](https://md.infs.ch/s/ROfGTLimc). 

### Testing

There are no automated tests for the plugin because there is are problems with make under Windows. Free free to contribute. 
What you can do is create a simple GeoJSON file (e.g. using geojson.io), which can contain just one point feature. 
The most important thing here is the filename.

### Releasing

When pushing to branch main a release is made. This release can be downloaded to directly uploaded to the QGIS plugin repository or direktly uploaded in QGIS. 


## License

 GPL3 (as required by QGIS).

## Authors

- [Niklas Vogel](https://github.com/Nukufel)
- [Diego Fantino](https://github.com/Fandiego)
- [Stefan Keller](https://gitlab.com/sfkeller)
- [Samuel Meuli](https://gitlab.ost.ch/ifs/projekte/-/project_members)
- [Mario Aeberhard](https://www.linkedin.com/in/marioaeberhard/)
- [Silvan Hegner](https://www.linkedin.com/in/silvan-hegner-7770ba258/)
- [OST-IFS](https://www.ost.ch/de/forschung-und-dienstleistungen/informatik/ifs-institut-fuer-software)


## Important Links

- Project repository: [https://gitlab.com/geometalab/aiamas](https://gitlab.com/geometalab/aiamas)
- QGIS Plugin inc. relases and other informations: See [metadata.txt](https://gitlab.com/geometalab/aiamas/aiamas-qgis-plugin/-/blob/main/AIAMAS/metadata.txt).
- Project documentation: [https://md.infs.ch/s/ROfGTLimc](https://md.infs.ch/s/ROfGTLimc)
- Project board issue: [https://gitlab.ost.ch/ifs/projekte/-/issues/206](https://gitlab.ost.ch/ifs/projekte/-/issues/206)
