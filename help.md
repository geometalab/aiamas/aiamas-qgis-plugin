# AIAMAS Help

## Short description

The AIAMAS plugin is an AI-assisted tool for rapid vector layer symbolization. It automatically suggests suitable colors and symbols for newly loaded vector layers in QGIS, based on the semantic meaning of the layer names. The plugin can assign a predefined entity (such as a bank, restaurant, etc.) that most closely resembles the layer name. The suggested styles – including colors and symbols – can be applied directly or customized with a few clicks. 

This plugin works for English, German, and French layer names, ensuring broad usability. However, it does not replace the QGIS Style Manager and is designed to work alongside it.

The AIAMAS plugin leverages a large language model (LLM) provided by the Institute for Software (IFS) at the Eastern Switzerland University of Applied Sciences (OST) to enhance its symbol and color suggestions. If the LLM service is unavailable, the plugin falls back to algorithmic string similarity methods to provide its recommendations.

## How to use the AIAMAS plugin

### 1. Installing and setting up

- Ensure the AIAMAS plugin is installed in QGIS (refer to the README.md for installation instructions via local zip file).
- Open QGIS and navigate to `Plugins > Manage and Install Plugins`. Enable AIAMAS from the list.
- The first time you use the plugin, an **API Token** must be entered in the QGIS Options dialog. You can obtain this token for free from the IFS LLMHub at <https://llmhub.infs.ch/>. Once you've created an account and generated the token, copy and paste it into the AIAMAS options dialog.

### 2. Adding and symbolizing a layer

- After loading a vector layer into QGIS, the AIAMAS plugin will automatically analyze the layer's name (e.g. "trees_Rapperswil_2024").
- The plugin chooses the best matching entity - such as "tree" - using the LLM, or a string similarity fallback method.
- Based on the matched entity, the plugin selects the corresponding, predefined color and symbol styles.
- You can now:
  - Accept the suggested styles with one click.
  - Adjust each colors and symbols manually if needed.
  - Or deselect any layer in order to leave the existing style untouched.

### 3. Language support

- The AIAMAS plugin currently supports name-to-entity matching in **English**, **German**, and **French**.
- It can handle also layer names in other languages but the quality of the suggestions may decrease.
- In any case ensure that the layer names are self-describing and meaningful.

### 4. Customizing a style

- The goal of this plugin is just to give a quick symbolization of vector layers.
- If you need full control over the style use the QGIS Style Manager.

### 5. API token management

- If at any time you need to update or change your API token, go to the **QGIS Options dialog** under the AIAMAS plugin section.
- Tokens can be regenerated at <https://llmhub.infs.ch/>.

## FAQ

### Q: How can I report style issues?
A: There is a tracker in the plugin repository for browsing and reporting bugs and suggestions: see "Reporting an issue" in "How to contribute" below. Describe the style issue and the suggested style improvements. If you have an issue with bad style suggestions, please go to the AIAMAS Options in the QGIS Preferences dialog, check "Verbose logging" and restart AIAMAS. Now you can copy and paste the log into your report.

### Q: How do I manually customize the styles suggested by the AIAMAS plugin?
A: After the plugin suggests a style, you can click on the QGIS Style Manager to change colours, symbols or other properties as needed.

### Q: What happens if there's no (valid) API token?
A: Without a (valid) API token, the plugin will revert to using algorithmic string similarity for style suggestions. Access to the online LLM with a valid API token enables semantic matching, which gives better results.

### Q: What do I do if the LLM service is unavailable?
A: If the LLM service is unavailable, the plugin switches to string matching of layer names to entities.

## How to contribute

We are always looking for feedback and contributions to improve the AIAMAS plugin. You can contribute by:

- **Reporting an issue**: If you encounter a bug or think of an improvement, submit an issue on the [GitLab issue board](https://gitlab.com/geometalab/aiamas/aiamas-qgis-plugin/-/issues).
- **Submitting merge requests**: If you’re a developer, feel free to clone the repository and create a merge request with any changes or enhancements.
- **Improving documentation**: Documentation is always a work in progress. If you spot areas that need clarification, please contribute to our [documentation site](https://md.infs.ch/s/ROfGTLimc).
