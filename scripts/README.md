## README for AIAMAS developer scripts

This folder is used for scripts that are only used by the developers.
Here we have a poetry environment that is used to run the scripts.
It is recommended to use the click library to create command line interfaces for the scripts.

### refactor command
To refactor the spatial_entities_colors_icons.json (sort entitites and change names to lowercase) run:

```poetry run conf refactor <api_key>```

This command will rewrite the spatial_entities_colors_icons.json, delete the current embeddings as well as generate new embeddings. This process will avoid possible issues.

### embed command
To embed newly added entities in the spatial_entities_colors_icons.json run:

```poetry run conf embed <api_key>```

This command will only embed the new entities in spatial_entities_colors_icons.json, it is recommended to use the refactor command, as it has the same functionality and avoids possible issues. However, it takes a little longer to reembed all entities.