import click
from tqdm import tqdm

import scripts.paths

from .embed_spatial_entities import \
    embed_spatial_entities as _embed_spatial_entities
from .refactor_spatial_entities import refactor


@click.group()
def commands():
    pass

# refactor the spatial_entities, delete current embeddings and reembed to avoid possible issues (run if elements were edited or to sort json)
@click.command("refactor", help='Refactor the spatial_entities_colors_icons.json (sort entitites and change names to lowercase): This command will rewrite the spatial_entities_colors_icons.json, delete the current embeddings as well as generate new embeddings. This process will avoid possible issues.')
@click.argument('api_key')
def refactor_spatial_entities_file(api_key):
    refactor()
    embedding_file = scripts.paths.EMBEDDINGS_FILE
    embedding_file.unlink(missing_ok=True)
    
    _embed_spatial_entities(api_key)

# embed spatial_entities if new elements were added (refactor if elements edited)
@click.command("embed", help='Embed newly added entities in the spatial_entities_colors_icons.json: This command will only embed the new entities in spatial_entities_colors_icons.json, it is recommended to use the refactor command, as it has the same functionality and avoids possible issues. However, it takes a little longer to reembed all entities.')
@click.argument('api_key')
def embed_spatial_entities(api_key):
    _embed_spatial_entities(api_key)


commands.add_command(refactor_spatial_entities_file)
commands.add_command(embed_spatial_entities)

if __name__ == '__main__':
    commands()
