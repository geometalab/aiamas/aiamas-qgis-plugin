from pathlib import Path

REPO_ROOT = Path(__file__).parent.parent.parent
DATA_DIR = REPO_ROOT / 'AIAMAS' / 'data'

ENTITIES_FILE = DATA_DIR / 'spatial_entities_colors_icons.json'
EMBEDDINGS_FILE = DATA_DIR / 'entity_embeddings.json'
