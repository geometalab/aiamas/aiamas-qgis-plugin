import json

import scripts.paths


def refactor():

    # define new data dictionary
    new_data = {
        "data": []
    }

    # read old file
    with open(scripts.paths.ENTITIES_FILE, 'r', encoding='utf-8') as file:
        old_file_data = json.load(file)

    # add elements in new order to data dictionary
    for spatial_entity in old_file_data['data']:
        new_element = {
            "name_en": [name.lower() for name in spatial_entity['name_en']],
            "name_de": [name.lower() for name in spatial_entity['name_de']],
            "name_fr": [name.lower() for name in spatial_entity['name_fr']],
            "color": [
                f"{spatial_entity['color'][0]}",
                f"{spatial_entity['color'][1]}",
                f"{spatial_entity['color'][2]}"
            ],
            "icons": [
                f"{spatial_entity['icons'][0]}",
                f"{spatial_entity['icons'][1]}",
                f"{spatial_entity['icons'][2]}"
            ]
        }

        new_data['data'].append(new_element)

    # sort new data dictionary by name_en
    new_data['data'] = sorted(new_data['data'], key=lambda x: x['name_en'][0])

    # write new data dictionary to new file
    with open(scripts.paths.ENTITIES_FILE, 'w', encoding='utf-8') as f:
        json.dump(new_data, f, indent=4, ensure_ascii=False)

    # print success
    print(f"refactored: {scripts.paths.ENTITIES_FILE}")
